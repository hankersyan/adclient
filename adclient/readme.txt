Prerequisite
	1> android 4.4 or later. (for the immersive mode)

Exclusive Mode
	1> root your device after install adclient apk.
	2> open windows command for adb commands.
	3> run "adb shell"
	4> run "su"
	5> run "pm disable com.android.systemui"
	6> root your device and you will get the exclusive mode.
	
User Manual
	1> change your server parameters in the file /sdcard/com.example.adclient/server.config
	2> exit_pwd is the password for quitting adclient application
	3> long press the qrcode image of adclient to open the exit dialog and input the exit_pwd.