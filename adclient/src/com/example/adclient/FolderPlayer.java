package com.example.adclient;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;
import android.widget.VideoView;

public class FolderPlayer {
	Context _context;
	String _folderPath;
	ArrayList<File> _files;
	int _currentIndex = 0;
	Random _rand;
	Handler _uiHandler;
	VideoView _videoView;

	public FolderPlayer(Context c, String folderPath, VideoView videoView) {
		_context = c;
		_folderPath = folderPath;
		_files = new ArrayList<File>();
		_uiHandler = new Handler();
		_rand = new Random();
		_videoView = videoView;
		_videoView
				.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

					@Override
					public void onCompletion(MediaPlayer arg0) {
						playNext();
					}
				});
		_videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {

			@Override
			public boolean onError(MediaPlayer arg0, int arg1, int arg2) {
				playNext();
				return false;
			}
		});
	}

	public void reset() {
		_files.clear();
	}

	void listf(String directoryName, ArrayList<File> files) {
		files.clear();

		File directory = new File(directoryName);

		// get all the files from a directory
		File[] fList = directory.listFiles();
		for (File file : fList) {
			if (file.isFile()) {
				files.add(file);
			} else if (file.isDirectory()) {
				listf(file.getAbsolutePath(), files);
			}
		}
	}

	void playFolder(String path) {

		listf(path, _files);

		playNext();
	}

	Runnable _restartPlayTask = new Runnable() {
		@Override
		public void run() {
			// this will run on the main UI thread
			playFolder(_folderPath);
		}
	};

	Runnable _waitForRestartTask = new Runnable() {
		@Override
		public void run() {
			try {
				Thread.sleep(6000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Going to Profile Data");
			_uiHandler.post(_restartPlayTask);
		}
	};

	void WaitingForRestart() {

		new Thread(_waitForRestartTask).start();
	}

	Runnable _playNextTask = new Runnable() {
		@Override
		public void run() {
			// this will run on the main UI thread
			playNext();
		}
	};

	Runnable _waitForPlayNextTask = new Runnable() {
		@Override
		public void run() {
			try {
				Thread.sleep(6000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Going to Profile Data");
			_uiHandler.post(_playNextTask);
		}
	};

	void WaitingForPlayNext() {

		new Thread(_waitForPlayNextTask).start();
	}

	@SuppressLint("DefaultLocale")
	void playNext() {
		if (_files.size() < 1) {
			WaitingForRestart();
			return;
		}

		if (_currentIndex >= _files.size())
			_currentIndex = 0;

		int step = _rand.nextInt(_files.size());

		_currentIndex += step;
		_currentIndex %= _files.size();

		File f = _files.get(_currentIndex);

		MainActivity mainActivity = (MainActivity) _context;
		mainActivity.playFile(f.getAbsolutePath(), "");
		String fname = f.getName().toUpperCase();
		Log.i("FolderPlayer", "playing " + f.getAbsolutePath());
		if (fname.endsWith(".JPG") || fname.endsWith(".PNG")
				|| fname.endsWith(".GIF")) {

			WaitingForPlayNext();
		} else if (fname.endsWith(".MP4") || fname.endsWith(".AVI")
				|| fname.endsWith(".MPG")) {
		} else {
			playNext();
		}
	}
}
