package com.example.adclient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import android.content.Context;
import android.os.Environment;

public class MyConfig {

	final static String CONFIG_FILE = "server.config";
	static final int PORT = 3096;

	public MyConfig(Context context) {
		_wifiSSID = context.getResources().getString(R.string.my_wifi);
		_wifiPWD = context.getResources().getString(R.string.my_wifi_pwd);
		_serverIP = context.getResources().getString(R.string.server_ip);
		_exitPWD = context.getResources().getString(R.string.exit_pwd);

		loadSettingsFromExternal(context);
	}

	String _wifiSSID = "";

	public String getWIFISSID() {
		return _wifiSSID;
	}

	public void setWIFISSID(String ssid) {
		_wifiSSID = ssid;
	}

	String _wifiPWD = "";

	public String getWIFIPWD() {
		return _wifiPWD;
	}

	public void setWIFIPWD(String pwd) {
		_wifiPWD = pwd;
	}

	String _serverIP = "";

	public String getServerIP() {
		return _serverIP;
	}

	public void setServerIP(String serverIP) {
		_serverIP = serverIP;
	}

	int _serverPort = PORT;

	public int getServerPort() {
		return _serverPort;
	}

	public void setServerPort(int serverPort) {
		_serverPort = serverPort;
	}

	int _serverUDPPort = PORT + 1;

	public int getServerUDPPort() {
		return _serverUDPPort;
	}

	public void setServerUDPPort(int serverUDPPort) {
		_serverUDPPort = serverUDPPort;
	}

	long _containerID = 0;

	public long getContainerID() {
		return _containerID;
	}

	public void setContainerID(long containerID) {
		_containerID = containerID;
	}

	String _exitPWD = "";

	public String getExitPWD() {
		return _exitPWD;
	}

	public void setExitPWD(String exitPWD) {
		_exitPWD = exitPWD;
	}

	String _barcodeFormat = "QR_CODE";

	public String getBarcodeFormat() {
		return _barcodeFormat;
	}

	public void setBarcodeFormat(String barcodeFormat) {
		_barcodeFormat = barcodeFormat;
	}

	int _barcodeWidth = 256;

	public int getBarcodeWidth() {
		return _barcodeWidth;
	}

	public void setBarcodeWidth(int barcodeWidth) {
		_barcodeWidth = barcodeWidth;
	}

	int _barcodeHeight = 256;

	public int getBarcodeHeight() {
		return _barcodeHeight;
	}

	public void setBarcodeHeight(int barcodeHeight) {
		_barcodeHeight = barcodeHeight;
	}

	int _barcodeRotation = 0;

	public int getBarcodeRotation() {
		return _barcodeRotation;
	}

	public void setBarcodeRotation(int barcodeRotation) {
		_barcodeRotation = barcodeRotation;
	}

	void loadSettingsFromExternal(Context context) {

		Properties prop = new Properties();
		InputStream input = null;
		OutputStream output = null;

		try {
			String filepath = getFileAbsolutePath(context, CONFIG_FILE);

			File f = new File(filepath);
			if (f.exists()) {
				input = new FileInputStream(filepath);

				// load a properties file
				prop.load(input);

				_wifiSSID = prop.getProperty("my_wifi");
				_wifiPWD = prop.getProperty("my_wifi_pwd");
				_serverIP = prop.getProperty("server_ip");
				_serverPort = getSafeInt(prop.getProperty("server_port"), PORT);
				_serverUDPPort = getSafeInt(prop.getProperty("server_udp_port"), PORT + 1);
				_containerID = getSafeInt(prop.getProperty("container_id"), 0);
				_exitPWD = prop.getProperty("exit_pwd");
				_barcodeFormat = prop.getProperty("barcode_format");
				_barcodeWidth = getSafeInt(prop.getProperty("barcode_width"), 256);
				_barcodeHeight = getSafeInt(prop.getProperty("barcode_height"), 256);
				_barcodeRotation = getSafeInt(prop.getProperty("barcode_rotation"), 0);
			} else {
				output = new FileOutputStream(filepath);

				// set the properties value
				prop.setProperty("my_wifi", _wifiSSID);
				prop.setProperty("my_wifi_pwd", _wifiPWD);
				prop.setProperty("server_ip", _serverIP);
				prop.setProperty("server_port", "" + _serverPort);
				prop.setProperty("server_udp_port", "" + _serverUDPPort);
				prop.setProperty("container_id", "" + _containerID);
				prop.setProperty("exit_pwd", _exitPWD);
				prop.setProperty("barcode_format", _barcodeFormat);
				prop.setProperty("barcode_width", ""+_barcodeWidth);
				prop.setProperty("barcode_height", ""+_barcodeHeight);
				prop.setProperty("barcode_rotation", ""+_barcodeRotation);

				// save properties to project root folder
				prop.store(output, null);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
					input = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (output != null) {
				try {
					output.close();
					output = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	int getSafeInt(String src, int defaultValue) {
		try {
			
			if(src != null && src.length() > 0)
				return Integer.parseInt(src);
			
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return defaultValue;
	}

	String getFileAbsolutePath(Context context, String filename) {
		String folder = Environment.getExternalStorageDirectory().toString();
		String filepath = folder + "/" + context.getPackageName() + "/"
				+ filename;
		return filepath;
	}
}
