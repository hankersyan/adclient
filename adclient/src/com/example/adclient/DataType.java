package com.example.adclient;

public class DataType {
	public final static int DTNone = 0;
	public final static int DTInt16 = 1;
	public final static int DTInt32 = 3;
	public final static int DTInt64 = 4;
	public final static int DTFloat = 5;
	public final static int DTDouble = 6;
	public final static int DTString = 7;
	public final static int DTByteArray = 8;
	public final static int DTCommand = 100;
	public final static int DTCommandProductShow = 101;
	public final static int DTCommandProductAdd = 102;
	public final static int DTCommandProductRemove = 103;
	public final static int DTCommandProductUpdate = 104;
	public final static int DTFileHeader = 200;
	public final static int DTFileHeaderAck = 201;
	public final static int DTFile = 202;
	public final static int DTFileAck = 203;
	public final static int DTFileEnd = 204;
	public final static int DTFileEndAck = 205;
	public final static int DTSettings = 400;
	public final static int DTSettingsAck = 401;
	public final static int DTProduct = 500;
}
