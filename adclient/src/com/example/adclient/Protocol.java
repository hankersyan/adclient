package com.example.adclient;

import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicReference;

public class Protocol {
	public final static int HeartBeatTimeout = 6000;
	public final static int FileSendBufferSize = 4096;
    public static byte[] ACCEPTED = new byte[] {'a',
        'c',
        'c',
        'e',
        'p',
        't',
        'e',
        'd'       };
    public static BaseData Accepted = new BaseData((int)DataType.DTString, ACCEPTED.length, ACCEPTED);
    //heart beat protocol
    public static byte[] HELLO = new byte[] {'h',
        'e',
        'l',
        'l',
        'o'      };
    public static BaseData Hello = new BaseData((int)DataType.DTString, HELLO.length, HELLO);
    public static byte[] ACTIVE = new byte[] {'a',
        'c',
        't',
        'i',
        'v',
        'e'       };
    public static BaseData Actived = new BaseData((int)DataType.DTString, ACTIVE.length, ACTIVE);
    public static int MaxHelloCount = 3;
    public static Product Convert2Product(byte[] buffer, AtomicReference<Integer> startPos)
    {
        try
        {
            Product pro = null;
            BaseData bd = new BaseData();
            startPos.set(startPos.get() + bd.FromStream(buffer, startPos.get()));
            if (bd.Type == (int)DataType.DTProduct)
            {
                pro = new Product();
                int pos = 0;
                BaseData bbdd = new BaseData();
                while (pos < bd.Length - 8)
                {
                    pos += bbdd.FromStream(bd.Value, pos);
                    if (bbdd.Type == (int)DataType.DTString)
                    {
                        String name = new String(bbdd.Value);
                        if(name.equalsIgnoreCase("ID")) {
                            BaseData id = new BaseData();
                            pos += id.FromStream(bd.Value, pos);
                            if (id.Type == (int)DataType.DTInt32)
                            {
                                pro.Id = Protocol.ToInt(id.Value);
                            }
                            else
                            { // error
                                return null;
                            }
                        } else if(name.equalsIgnoreCase("NAME")) {
                            BaseData nn = new BaseData();
                            pos += nn.FromStream(bd.Value, pos);
                            if (nn.Type == (int)DataType.DTString)
                            {
                                pro.Name = new String(nn.Value);
                            }
                            else
                            { // error
                                return null;
                            }
                        } else if(name.equalsIgnoreCase("PictureName")) {
                            BaseData pic = new BaseData();
                            pos += pic.FromStream(bd.Value, pos);
                            if (pic.Type == (int)DataType.DTString)
                            {
                                pro.Picture = new String(pic.Value);
                            }
                            else
                            { // error
                                return null;
                            }
                        } else if(name.equalsIgnoreCase("ADFileName")) {
                            BaseData adf = new BaseData();
                            pos += adf.FromStream(bd.Value, pos);
                            if (adf.Type == (int)DataType.DTString)
                            {
                                pro.AdFile = new String(adf.Value);
                            }
                            else
                            { // error
                                return null;
                            }
                        } else if(name.equalsIgnoreCase("Description")) {
                            BaseData des = new BaseData();
                            pos += des.FromStream(bd.Value, pos);
                            if (des.Type == (int)DataType.DTString)
                            {
                                pro.Description = new String(des.Value);
                            }
                            else
                            { // error
                                return null;
                            }
                        }
                    }

                }

            }

            return pro;
        }
        catch (Exception exp)
        {
        	exp.printStackTrace();
        }
        return null;
    }

    public static byte[] ToByteArray(int intValue)
    {
        return ByteBuffer.allocate(4).putInt(intValue).array();
    }

    public static int ToInt(byte[] bytes)
    {
        return ByteBuffer.wrap(bytes).getInt();
    }
}
