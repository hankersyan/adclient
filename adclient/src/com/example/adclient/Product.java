package com.example.adclient;

import java.io.Serializable;

public class Product implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1429410377L;
	
	public Product() {

	}

	public Product(int id, String name, String picture) {
		Id = id;
		Name = name;
		Picture = picture;
	}

	public int Id;
	public String Name;
	public String Picture;
	public String AdFile;
	public String Description;
	
//	public int getId() {
//		return Id;
//	}
//
//	public void setId(int id) {
//		Id = id;
//	}
//
//	public String getName() {
//		return Name;
//	}
//
//	public void setName(String name) {
//		Name = name;
//	}
//
//	public String getPicture() {
//		return Picture;
//	}
//
//	public void setPicture(String picture) {
//		Picture = picture;
//	}
//
//	public String getAdFile() {
//		return AdFile;
//	}
//
//	public void setAdFile(String adFile) {
//		AdFile = adFile;
//	}
//
//	public String getDescription() {
//		return Description;
//	}
//
//	public void setDescription(String description) {
//		Description = description;
//	}
}
