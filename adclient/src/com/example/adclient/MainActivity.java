package com.example.adclient;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.ByteMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class MainActivity extends ImmersiveActivity {

	static final int REQUEST_ENABLE_BT = 1234;
	static final int REQUEST_EXIT = 999;
	
	// final static String CONTENT_PATH = "/storage/extSdCard/Download/";
	ImageView _imgView;
	VideoView _videoView;
	FrameLayout _videoFrame;
	NetClient _netClient = null;
	ViewGroup _sidePanel;
	TextView _txtQRCode;
	ImageView _imgQRCode;
	//HomeKeyLocker mHomeKeyLocker;
	MyConfig _myconfig;
	NearestBeaconMonitor _beaconMonitor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		_netClient = new NetClient(MainActivity.this);
		//mHomeKeyLocker = new HomeKeyLocker();
	}

	@Override
	protected void onStart() {
		super.onStart();

		_imgView = (ImageView) findViewById(R.id.imgView);
		_videoFrame = (FrameLayout) findViewById(R.id.videoFrame);
		_videoView = (VideoView) findViewById(R.id.videoView);
		_videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mp) {
				mp.setLooping(true);
			}
		});
		_sidePanel = (ViewGroup) findViewById(R.id.side_panel_coupon);
		_txtQRCode = (TextView) findViewById(R.id.txtQRCode);
		_imgQRCode = (ImageView) findViewById(R.id.imgQRCode);

		_beaconMonitor = new NearestBeaconMonitor(this);
		_beaconMonitor.setNearestBeaconListener(new NearestBeaconListener(){

			@Override
			public void onNearestBeaconFound(String uuid, int major, int minor) {

				Log.i("Nearest Beacon", "major="+major+",minor="+minor);
				
				NetClient.PlayInfo pi = _netClient.getProductPlayInfo(minor);
				
				if(pi != null) {
					showADonUIThread(pi.filepath, pi.description);
				}
			}
			
		});
		if (!_beaconMonitor.hasBluetoothle()) {
			Toast.makeText(this, "该设备没有蓝牙BLE!", Toast.LENGTH_LONG).show();
		} else {

			// 如果未打开蓝牙，则请求打开蓝牙。
			if (!_beaconMonitor.isBluetoothEnabled()) {
				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			} else {
				_beaconMonitor.start();
			}
		}
		
        setupExitDialog();

		//mHomeKeyLocker.lock(this);

		// playFolder(CONTENT_PATH);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		_myconfig = new MyConfig(MainActivity.this);

		new Thread(new Runnable() {

			@Override
			public void run() {
				_netClient.reload();
				connectToSpecifiedWifi(new Runnable() {

					@Override
					public void run() {
						_netClient.Loop();
					}
				});
			}
		}).start();
	}

	@Override
	protected void onPause() {
		super.onPause();
		_netClient.save();
		_netClient.stop();
		
		_beaconMonitor.close();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		//mHomeKeyLocker.unlock();
		//mHomeKeyLocker = null;
	}

	public void showADonUIThread(final String fileName, final String qrCode) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				playFile(fileName, qrCode);
			}
		});
	}

	@SuppressLint("DefaultLocale")
	public void playFile(final String filepath, final String qrCode) {
		String fname = filepath.toUpperCase();
		Log.i("adclient", "playing " + filepath);
		if (fname.endsWith(".JPG") || fname.endsWith(".PNG")
				|| fname.endsWith(".GIF")) {

			_imgView.setImageURI(Uri.parse(filepath));
			_imgView.setVisibility(View.VISIBLE);
			_videoView.stopPlayback();
			_videoView.setVisibility(View.GONE);
			_videoFrame.setVisibility(View.GONE);
		} else if (fname.endsWith(".MP4") || fname.endsWith(".AVI")
				|| fname.endsWith(".MPG")) {
			_imgView.setVisibility(View.GONE);
			_videoFrame.setVisibility(View.VISIBLE);
			_videoView.setVisibility(View.VISIBLE);
			_videoView.setVideoURI(Uri.parse(filepath));
			_videoView.start();
			_videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
				
				@Override
				public void onCompletion(MediaPlayer mp) {
					_videoView.setVideoURI(Uri.parse(filepath));  
					_videoView.start();  
				}
			});
		} else {
		}

		if (qrCode == null || qrCode.equals("")) {
			_sidePanel.setVisibility(View.GONE);
		} else {
			_sidePanel.setVisibility(View.VISIBLE);
			_txtQRCode.setText(qrCode);
			new Thread(new Runnable() {
				@Override
				public void run() {
					generateQRCode(qrCode);
				}
			}).start();
		}
	}

	void generateQRCode(String qrcode) {
		QRCodeWriter writer = new QRCodeWriter();
		final int W = _myconfig.getBarcodeWidth();
		final int H = _myconfig.getBarcodeHeight();
		try {
			BarcodeFormat barcodeFmt = getBarcodeFormat();
			
			ByteMatrix bitMatrix = writer.encode(qrcode, barcodeFmt,
					W, H);
			int width = W;
			int height = H;
			Bitmap bmp = Bitmap.createBitmap(width, height,
					Bitmap.Config.RGB_565);
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					if (bitMatrix.get(x, y) == 0)
						bmp.setPixel(x, y, Color.BLACK);
					else
						bmp.setPixel(x, y, Color.WHITE);
				}
			}
			final Bitmap img = bmp;
			MainActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					_imgQRCode.setImageBitmap(img);
					_imgQRCode.setRotation(_myconfig.getBarcodeRotation());
				}
			});
		} catch (WriterException e) {
			// Log.e("QR ERROR", ""+e);

		}
	}

	void connectToSpecifiedWifi(Runnable successRunner) {

		String ssid = _myconfig.getWIFISSID();
		WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		if(!wifiManager.isWifiEnabled()) {
			// for emulator
			System.out.println("it is an emulator");
			successRunner.run();
		}
		else if (wifiInfo != null 
				&& wifiInfo.getSSID().replace("\"", "").equalsIgnoreCase(ssid)
				&& wifiInfo.getSupplicantState() == SupplicantState.COMPLETED) {
			// existed WIFI connection
			System.out.println("Connection to [" + ssid + "] exists");
			successRunner.run();
		} else {
			String pwd = _myconfig.getWIFIPWD();
			
			// we create new WIFI connection.
			WifiConfiguration wifiConfig = new WifiConfiguration();
			wifiConfig.SSID = String.format("\"%s\"", ssid);
			wifiConfig.preSharedKey = String.format("\"%s\"", pwd);

			int netId = wifiManager.addNetwork(wifiConfig);
			wifiManager.disconnect();
			Boolean ret = wifiManager.enableNetwork(netId, true);
			ret &= wifiManager.reconnect();
			if (ret) {
				while (true) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					wifiInfo = wifiManager.getConnectionInfo();
					if (wifiInfo != null
							&& (wifiInfo.getSupplicantState() == SupplicantState.COMPLETED
									|| wifiInfo.getSupplicantState() == SupplicantState.INACTIVE
									|| wifiInfo.getSupplicantState() == SupplicantState.INVALID
									|| wifiInfo.getSupplicantState() == SupplicantState.DISCONNECTED || wifiInfo
									.getSupplicantState() == SupplicantState.UNINITIALIZED)) {
						break;
					}
				}
			}
			if (wifiInfo.getSupplicantState() == SupplicantState.COMPLETED
					&& wifiInfo.getSSID().replace("\"", "").equalsIgnoreCase(ssid)) {
				successRunner.run();
			} else {
				MainActivity.this.runOnUiThread(new Runnable(){

					@Override
					public void run() {
						String msg = getResources().getString(
								R.string.failed_to_connect_my_wifi);
						_txtQRCode.setText(msg);
						Toast.makeText(
								MainActivity.this,
								msg,
								Toast.LENGTH_LONG).show();
					}});
			}
		}
	}

	void setupExitDialog() {
        _imgQRCode.setOnLongClickListener(new OnLongClickListener(){

			@Override
			public boolean onLongClick(View arg0) {
				
                Intent myIntent = new Intent(MainActivity.this, ExitActivity.class);
                MainActivity.this.startActivityForResult(myIntent, REQUEST_EXIT);

				return true;
			}
			
		});		
	}
	
	// Dialog will show the navigation bar of system.
	void popupExitDiaglog() {
		
//		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//		builder.setTitle("Exit");
//
//		// Set up the input
//		final EditText input = new EditText(MainActivity.this);
//		// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
//		input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//		builder.setView(input);
//
//		// Set up the buttons
//		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//		    @Override
//		    public void onClick(DialogInterface dialog, int which) {
//		        if(input.getText().toString().equals(new MyConfig(MainActivity.this).getExitPWD())) {
//		        	finish();
//		        }
//		    }
//		});
//		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//		    @Override
//		    public void onClick(DialogInterface dialog, int which) {
//		        dialog.cancel();
//		    }
//		});
//
//		builder.show();
	}

	BarcodeFormat getBarcodeFormat() {
		String bcFmt = _myconfig.getBarcodeFormat();
		
		if(bcFmt.equalsIgnoreCase("code_128")) {
			return BarcodeFormat.CODE_128;
		} else if(bcFmt.equalsIgnoreCase("code_39")) {
			return BarcodeFormat.CODE_39;
		} else if(bcFmt.equalsIgnoreCase("datamatrix")) {
			return BarcodeFormat.DATAMATRIX;
		} else if(bcFmt.equalsIgnoreCase("ean_13")) {
			return BarcodeFormat.EAN_13;
		} else if(bcFmt.equalsIgnoreCase("ean_8")) {
			return BarcodeFormat.EAN_8;
		} else if(bcFmt.equalsIgnoreCase("itf")) {
			return BarcodeFormat.ITF;
		} else if(bcFmt.equalsIgnoreCase("upc_a")) {
			return BarcodeFormat.UPC_A;
		} else if(bcFmt.equalsIgnoreCase("upc_e")) {
			return BarcodeFormat.UPC_E;
		}
		
		return BarcodeFormat.QR_CODE;
	}

	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_EXIT) {
            if (resultCode == RESULT_OK) {
                String msg = data.getStringExtra("exit");
                if(msg.equals("1")) {
                    this.finish();
                }
            }
        } else if (requestCode == REQUEST_ENABLE_BT) {
			if (resultCode == RESULT_OK) {
				_beaconMonitor.start();
			} else {
				Toast.makeText(this, "设备蓝牙未打开", Toast.LENGTH_LONG).show();
			}
		}
    }
    
}
