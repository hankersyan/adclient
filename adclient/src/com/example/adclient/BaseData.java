package com.example.adclient;

import java.nio.ByteBuffer;

public class BaseData {
	public BaseData() {
		Type = 0;
		Length = 0;
		Value = null;
	}
	public BaseData(int _type, int _length, byte[] _value) {
		Type = _type;
		Length = _length;
		if(_value == null)
			Value = new byte[0];
		else
			Value = _value;
	}
	public int Type;
	public int Length;
	public byte[] Value;
/*	int type;
	public int getType() {
		return type;
	}
	public void setType(int _type) {
		type = _type;
	}
	int length;
	public int getLength() {
		return length;
	}
	public void setLength(int _length) {
		length = _length;
	}
	byte[] value;
	public byte[] getValue() {
		return value;
	}
	public void setValue(byte[] _value) {
		value = _value;
	}
*/
	public byte[] ToStream() {
		ByteBuffer buf = ByteBuffer.allocate(4*2 + Length);
		buf.putInt(Type);
		buf.putInt(Length);
		buf.put(Value);
		return buf.array();
	}
	public int FromStream(byte[] buf, int startPos) {
		Type = ByteBuffer.wrap(buf, startPos, 4).getInt();
		Length = ByteBuffer.wrap(buf, startPos + 4, 4).getInt();
		Value = new byte[Length];
		System.arraycopy(buf, startPos + 8, Value, 0, Length);
		//Value = Arrays.copyOfRange(buf, startPos + 8, buf.length - 8);
		return 4 + 4 + Length;
	}
}
