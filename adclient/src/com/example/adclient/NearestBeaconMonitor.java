package com.example.adclient;

import android.app.Activity;
import android.os.RemoteException;
import com.brtbeacon.sdk.BRTBeaconManager;
import com.brtbeacon.sdk.BRTRegion;
import com.brtbeacon.sdk.RangingListener;
import com.brtbeacon.sdk.ServiceReadyCallback;
import com.brtbeacon.sdk.service.RangingResult;

public class NearestBeaconMonitor {
	
	static final String TIM_GUID = "31313131-3232-3232-3333-333334343434";
	static final BRTRegion ALL_TIM_BEACONS_REGION = new BRTRegion("rid", TIM_GUID, null, null, null);

	Activity _activity;
	BRTBeaconManager _beaconManager;
	NearestBeaconListener _nearestBeaconListener;

	public NearestBeaconMonitor(Activity c) {
		
		_activity = c;
		
		BRTBeaconManager.registerApp(_activity, "00000000000000000000000000000000");
		
		_beaconManager = new BRTBeaconManager(_activity);
		
		_beaconManager.setRangingListener(new RangingListener() {

			@Override
			public void onBeaconsDiscovered(final RangingResult rangingResult) {

				_activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						//
						if(rangingResult.beacons.size() > 0) {
							String uuid = rangingResult.beacons.get(0).getUuid();
							int major = rangingResult.beacons.get(0).getMajor();
							int minor = rangingResult.beacons.get(0).getMinor();
							if(_nearestBeaconListener != null) {
								_nearestBeaconListener.onNearestBeaconFound(uuid, major, minor);
							}
						}
					}
				});
			}

		});
		
	}
	
	public void setNearestBeaconListener(NearestBeaconListener nearestBeaconListener) {
		_nearestBeaconListener = nearestBeaconListener;
	}
	
	public boolean hasBluetoothle() {
		return _beaconManager.hasBluetoothle();
	}
	public boolean isBluetoothEnabled() {
		return _beaconManager.isBluetoothEnabled();
	}
	public void start() {
		
		connectToService();
	}
	
	private void connectToService() {

		// 扫描之前先建立扫描服务
		_beaconManager.connect(new ServiceReadyCallback() {
			@Override
			public void onServiceReady() {
				try {
					// 开始扫描
					_beaconManager.startRanging(ALL_TIM_BEACONS_REGION);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void close() {
		
		try {
			_beaconManager.stopRanging(ALL_TIM_BEACONS_REGION);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		_beaconManager.disconnect();
	}
}
