package com.example.adclient;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class ExitActivity extends ImmersiveActivity {

    TextView _txtPwd;
    String _strPwd = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exit);
        _txtPwd = (TextView) findViewById(R.id.txt_exit_pwd);
    }

    public void clickNumber(View sender) {
        if(_strPwd.length() < 6) {
            Button btn = (Button) sender;
            _strPwd += btn.getText().toString();
            _txtPwd.setText("******".substring(0, _strPwd.length()));
        }
    }

    public void clickClear(View sender) {
        _strPwd = "";
        _txtPwd.setText("");
    }

    public void clickBack(View sender) {
        if(_strPwd.length() > 0) {
            _strPwd = _strPwd.substring(0, _strPwd.length() - 1);
            _txtPwd.setText("******".substring(0, _strPwd.length()));
        } else {
            _strPwd = "";
            _txtPwd.setText("");
        }
    }

    public void clickOK(View sender) {
        if(_strPwd.equals(new MyConfig(this).getExitPWD())) {
            Intent intent = this.getIntent();
            intent.putExtra("exit", "1");
            this.setResult(RESULT_OK, intent);
            finish();
        }
    }

    public void clickCancel(View sender) {
        this.finish();
    }
}
