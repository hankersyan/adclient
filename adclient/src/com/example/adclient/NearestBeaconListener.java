package com.example.adclient;

public abstract class NearestBeaconListener {

	public abstract void onNearestBeaconFound(String uuid, int major, int minor);
}
